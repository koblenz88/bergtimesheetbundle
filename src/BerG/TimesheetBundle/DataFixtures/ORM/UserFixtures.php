<?php
// src/BerG/TimesheetBundle/DataFixtures/ORM/UserFixtures.php

namespace Berg\TimesheetBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use BerG\TimesheetBundle\Entity\User;

class UserFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setEmployedFrom(new \DateTime('2015-04-14'))
            ->setEmployedTo(new \DateTime('2016-04-14'))
            ->setIsEmployed(true)
            ->setUsername('Mojo')
            ->setEmail('mojo@ts.com')
            ->setPlainPassword('12345678')
            ->setLastSubmittedDay($user1->getEmployedFrom())
            ->addRole('ROLE_MANAGER')
            ->setEnabled(true)
        ;
        $manager->persist($user1);

        
        $user2 = new User();
        $user2->setEmployedFrom(new \DateTime('2015-06-14'))
            ->setEmployedTo(new \DateTime('2016-06-14'))
            ->setIsEmployed(true)
            ->setUsername('tester')
            ->setEmail('tester@ts.com')
            ->setPlainPassword('12345678')
            ->setLastSubmittedDay($user2->getEmployedFrom())
        //    ->addRole('ROLE_MANAGER')
            ->setEnabled(true)
        ;
        $manager->persist($user2);

        
        $user3 = new User();
        $user3->setEmployedFrom(new \DateTime('2015-05-24'))
            ->setEmployedTo(new \DateTime('2016-05-24'))
            ->setIsEmployed(true)
            ->setUsername('chef')
            ->setEmail('chef@ts.com')
            ->setPlainPassword('12345678')
            ->setLastSubmittedDay($user3->getEmployedFrom())
            ->addRole('ROLE_MANAGER')
            ->setEnabled(true)
        ;
        $manager->persist($user3);

        $user4 = new User();
        $user4->setEmployedFrom(new \DateTime('2015-07-21'))
            ->setEmployedTo(new \DateTime('2016-07-21'))
            ->setIsEmployed(true)
            ->setUsername('worker')
            ->setEmail('worker@ts.com')
            ->setPlainPassword('12345678')
            ->setLastSubmittedDay($user4->getEmployedFrom())
            //    ->addRole('ROLE_MANAGER')
            ->setEnabled(true)
        ;
        $manager->persist($user4);
        
        $manager->flush();
    }
}
