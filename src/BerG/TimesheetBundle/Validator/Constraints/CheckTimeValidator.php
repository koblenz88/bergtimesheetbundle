<?php

namespace BerG\TimesheetBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckTimeValidator extends ConstraintValidator
{
    public function validate($foo, Constraint $constraint)
    {
            if (!($foo->getEndTime() > $foo->getStartTime())) {
                $this->context->buildViolation($constraint->message)
                        ->atPath('end_time')
                        //->setParameter('%string%', 'end_time')
                        ->addViolation();
            }
    }
}
