<?php

namespace BerG\TimesheetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeekType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('days', 'collection', ['type' => new DayType()])
            ->add('submit', 'submit', ['label' => 'Submit this Week'])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BerG\TimesheetBundle\Entity\Week'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'week';
    }
}
