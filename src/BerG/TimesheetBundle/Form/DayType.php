<?php

namespace BerG\TimesheetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DayType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $builder
                ->add('dayDate', 'date', [
                    //'data_class' => 'Carbon\Carbon',
                    'disabled' => true,
                ])
                ->add('startHour', 'time')
                ->add('endHour', 'time')
                ->add('hours', 'time')
            ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BerG\TimesheetBundle\Entity\Day',
        ));
    }
    /**
     * @return string
     */
    public function getName()
    {
        return 'day';
    }
}
