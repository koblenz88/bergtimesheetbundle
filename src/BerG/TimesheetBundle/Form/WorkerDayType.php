<?php

namespace BerG\TimesheetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkerDayType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            $builder
                ->add('startHour', 'time')
                ->add('endHour', 'time')
                //->add('month', 'hidden')
                ->add('dayDate', 'hidden', [
                    'data_class' => 'Carbon\Carbon'
                ])

                //, 'sub-form'
            ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BerG\TimesheetBundle\Entity\WorkerDay',
        ));
    }
    /**
     * @return string
     */
    public function getName()
    {
        return 'ber_g_timesheetbundle_workerday';
    }
}
