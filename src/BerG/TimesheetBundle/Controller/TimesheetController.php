<?php

namespace BerG\TimesheetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BerG\TimesheetBundle\Form\WorkerDayType;
//use BerG\TimesheetBundle\Entity\Weekform;
//use BerG\TimesheetBundle\Entity\WorkerDay;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;
/**
 * Timesheet Controller
 */
class TimesheetController extends Controller
{
    public function loginAction()
    {
        return $this->forward('FOSUserBundle:Security:login');
    }
    /**
     * Show pending week timesheet forms
     * @return type
     * @throws type
     */
    public function timesheetAction()
    {
        //$now = Carbon::now();
        $lastLimit = new Carbon('last friday');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        //dump($user->getRoles());

        $repoweek = $em->getRepository('BerGTimesheetBundle:WorkerDay');

        $submitted = $repoweek->findBy([
                'worker' => $user,
                //'isSubmitted' => true,
                'year' => $lastLimit->year,
                'week' => $lastLimit->weekOfYear,
            ])
        ;

        if ($submitted)
        {
            return $this->render('BerGTimesheetBundle:Workerday:weeksheets.html.twig', array(
                'weeksheets' => [],
                'myexception' => 'No pending timesheets to dispaly. Please wait for the new week.\n',
            ));
        }

        $notsubmitted = $repoweek->findBy([
                'worker' => $user,
                //'isSubmitted' => false,
                'year' => $lastLimit->year,
                'week' => $lastLimit->weekOfYear,
            ])
        ;

        if ($notsubmitted)
        {
            $weeksheets1 = $notsubmitted;
        }
        else
        {
            $weeksheets1 = $repoweek->addWorkingWeek($lastLimit, $em, $user);
        }

        $weeksheets = [];
        //foreach ($weeksheets1 as $w)
        //{
        $w = $weeksheets1[0];

        $weeksheets[] = [
            'year' => $w->getYear(),
            'week_id' => $w->getWeekOfYear(),
            'start_date' => $w->getDayDate(),
            'end_date' => $weeksheets1[count($weeksheets1)-1]->getDayDate(),
        ];
        //}
        //dump($weeksheets);

        return $this->render('BerGTimesheetBundle:Workerday:weeksheets.html.twig', array(
            'weeksheets' => $weeksheets,
        ));
    }
    /**
     * Show Weekform
     * @param Weekform $weekform
     * @return type
     */
    public function weekformAction($year_id, $week_id)
    {
        //$weekform = new Weekform();//TODO instead get the days!
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repoweek = $em->getRepository('BerGTimesheetBundle:WorkerDay');

        $weekform = $repoweek->findBy([
                'worker' => $user,
                //'isSubmitted' => false,
                'year' => $year_id,
                'week' => $week_id,
            ])
        ;
        //dump($weekform);die();
        $form = $this->createForm(new WorkerDayType(), $weekform);

        return $this->render('BerGTimesheetBundle:Weekform:weekform.html.twig', array(
            'form' => $form->createView(),
            //'date' => $weekform,
        ));
    }
    /**
     * Submit Weekform
     * @param type $form_id
     */
    public function submitWeekformAction(Request $request, $year_id, $week_id)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repoweek = $em->getRepository('BerGTimesheetBundle:WorkerDay');

        $weekform = $repoweek->findBy([
                'worker' => $user,
                //'isSubmitted' => false,
                'year' => $year_id,
                'week' => $week_id,
            ])
        ;
        $form = $this->createForm(new WorkerDayType(), $weekform);

        $form->handleRequest($request);

        if ($form->isValid())
        {
            //TODO persist weekform in days
            $em = $this->getDoctrine()
						->getManager();

            foreach ($weekform as $w)
            {
                $em->persist($w);
            }

			$em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('New Timesheet submitted!')
                ->setFrom('weekform@timesheet.com')
                //->setTo($this->container->getParameter('ber_g_timesheet.emails.contact_email'))
                ->setBody($this->renderView('BerGTimesheetBundle:Weekform:weekformEmail.txt.twig', array('weekform' => $weekform)));

//TODO send to all roles ROLE_MANAGER
            $r = $em->getRepository('BerGTimesheetBundle:User');
            $managers = $r->findByRole('ROLE_MANAGER');
            foreach ($managers as $m)
            {
                $message->setTo($m->getEmail());
            }

            $this->get('mailer')->send($message);

            $this->get('session')->getFlashBag()->add('ts-notice', 'Your Timesheet was successfully submitted. Thank You!');

            // Redirect - This is important to prevent users re-posting
            // the form if they refresh the page
            return $this->redirect($this->generateUrl('ber_g_timesheet_timesheets'));
        }

        return $this->render('BerGTimesheetBundle:Weekform:weekform.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
