<?php

namespace BerG\TimesheetBundle\Controller;

use BerG\TimesheetBundle\Form\WeekType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BerG\TimesheetBundle\Entity\Week;
use Symfony\Component\HttpFoundation\Request;

class WeekController extends Controller
{
    public function newAction()//User $user)
    {
        $user = $this->getUser();
        $week = new Week();
        $week->setOwner($user);
        $form = $this->createForm(new WeekType(), $week);


        return $this->render('BerGTimesheetBundle:Week:new.html.twig', array(
            'week' => $week,
            'form' => $form->createView(),
        ));
    }

    public function createAction(Request $request)
    {
        $user = $this->getUser();
        $week = new Week();
        $week->setOwner($user);
        $week->makeSomeDays();
        $form = $this->createForm(new WeekType(), $week);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()
						->getManager();

			$em->persist($week);
			$em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('New Timesheet submitted!')
                ->setFrom('weekform@timesheet.com')
                ->setBody($this->renderView('BerGTimesheetBundle:Weekform:weekformEmail.txt.twig', array('weekform' => $week)));

            //send to all roles ROLE_MANAGER
            $r = $em->getRepository('BerGTimesheetBundle:User');
            $managers = $r->findByRole('ROLE_MANAGER');
            foreach ($managers as $m)
            {
                $message->setTo($m->getEmail());
            }

            $this->get('mailer')->send($message);

            $this->get('session')->getFlashBag()->add('ts-notice', 'Your Timesheet was successfully submitted. Thank You!');


            return $this->redirect(
                $this->generateUrl(
                    'show', array(

                    )
                )
            );
        }

        return $this->render('BerGTimesheetBundle:Week:create.html.twig', array(
            'week' => $week,
            'form' => $form->createView(),
        ));
    }

    /**\
     * if authenticated show list of weeks and days
     * if not redirect to /login
     */
    public function indexAction()
    {
        return $this->render('BerGTimesheetBundle:Week:index.html.twig', array(
            // ...
        ));
    }

    public function showAction()
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('New Timesheet submitted!')
            ->setFrom('weekform@timesheet.com')
            ->setBody($this->renderView('BerGTimesheetBundle:Weekform:weekformEmail.txt.twig', array()));//'weekform' => $week
        
        $message->setTo("gabriel.bernacki@gmail.com");//$m->getEmail()
        
        $this->get('mailer')->send($message);
        
        return $this->render('BerGTimesheetBundle:Week:show.html.twig', array(
            // ...
        ));
    }

}
