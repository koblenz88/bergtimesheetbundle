<?php

namespace BerG\TimesheetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BerG\TimesheetBundle\Entity\Week;
use BerG\TimesheetBundle\Entity\Day;
use BerG\TimesheetBundle\Form\WeekType;
use BerG\TimesheetBundle\Entity\User;

use Carbon\Carbon;

class TimesheetsController extends Controller
{
    /*
     * Home view
     * for anonymous: link to timesheets, link to newest pending timesheet
     * for USER: tsaa
     * for MANAGER: tsaa, link to users, link to months, 10 links to newest
     */
    public function indexAction()
    {
        return $this->render('BerGTimesheetBundle:Timesheets:index.html.twig', array(
                // ...
            ));    }
    /*
     * List of pending timesheets 
     */
    public function timesheetsAction($mode)
    {        
        switch ($mode)
        {//TODO: add limits and paging
            case 'open':
                $showOpens = true;
                $showClosed = false;
                break;
            case 'closed':
                $showOpens = false;
                $showClosed = true;
                break;
            case 'overview';
            default:
                $showOpens = true;
                $showClosed = true;
                break;
        }

        $user = $this->getUser();
        
        if ($showOpens)
        {
            $opens = $user->getOpenTimesheetsDates();
        }
        else
        {
            $opens = [];
        }
        
        if ($showClosed)
        {
            //get from user
        }
        else
        {
            $closed = [];
        }
        
        return $this->render('BerGTimesheetBundle:Timesheets:list.html.twig', array(
            'opens' => $opens,
            'showOpens' => $showOpens,
            'closed' => $closed,
            'showClosed' => $showClosed,
        ));
    }

    /**
     * Timesheet form
     * @param Request $request
     * @param User    $user
     * @return
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();

        $friday_of_unsubmitted_week = $request->query->has('w') ?
            Carbon::parse($request->query->get('w')) :
            Carbon::instance($user->getLastSubmittedDay())->next();

        $friday_of_unsubmitted_week = $user->getNextFridayIfNotFriday($friday_of_unsubmitted_week);
        
        $monday_of_unsubmitted_week = $friday_of_unsubmitted_week->copy()
            ->previous(Carbon::MONDAY)->max($user->getEmployedFrom(1));
        
        $week = new Week();
        for (
            $day = $monday_of_unsubmitted_week->copy();
            $day->lt($friday_of_unsubmitted_week) || $day->eq($friday_of_unsubmitted_week);
            $day->addDay()
        )
        {
            dump($day);
            $realDay = new Day();
            $realDay->setDayDate($day->copy());
            $t = new \DateTime('08:00:00');
            $te = new \DateTime('16:00:00');
            $realDay->setStartHour($t);
            $realDay->setEndHour($te);
            $week->addDay($realDay);
        }
        
        $form = $this->createForm(new WeekType(), $week);

        return $this->render('BerGTimesheetBundle:Week:new.html.twig', array(
            //'week' => $week,
            'form' => $form->createView(),
        ));
    }
    /**
     * Timesheet submit form
     */
    public function createAction(Request $request)
    {
        $week = new Week();

        $form = $this->createForm(new WeekType(), $week);
        
        $form->handleRequest($request);//recognizes if the form was submitted or not
        
        if ($form->isValid())
        {
            //perform some action like saving data, sending email
            
            return $this->redirectToRoute('view_timesheet');//with param which ts to show
        }
        //what else if form is not valid?
        //rendering the form with validation errors
        return $this->render('BerGTimesheetBundle:Week:new.html.twig', array(
            //'week' => $week,
            'form' => $form->createView(),
        ));
    }
}
