<?php

namespace BerG\TimesheetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Exception\ValidatorException;

//use BerG\TimesheetBundle\Validator\Constraints as AcmeAssert;
use BerG\TimesheetBundle\Entity\User;
use Carbon\Carbon;

/**
 * Day
 *
 * @ORM\Table(name="ts_days")
 * @ORM\Entity(repositoryClass="BerG\TimesheetBundle\Entity\Repository\DayRepository")
 * @ORM\HasLifecycleCallbacks
 * AcmeAssert\CheckTime
 */
class Day
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetimetz")
     * @Assert\Date()
     */
    protected $dayDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="time", nullable=true)
     * @Assert\Time()
     */
    protected $startHour;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="time", nullable=true)
     * @Assert\Time()
     */
    protected $endHour;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="days")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    /**
     * @var \Time
     *
     * @ORM\Column(type="time", nullable=true)
     * @Assert\Time()
     */
    protected $hours;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;
    /**
     * @ORM\Column(type="integer")
     */
    protected $month;
    /**
     * @ORM\Column(type="integer")
     */
    protected $year;
    /**
     * @ORM\ManyToOne(targetEntity="Week", inversedBy="days")
     * @ORM\JoinColumn(name="week_id", referencedColumnName="id")
     */
    protected $week;
    /**
     * @ORM\ManyToOne(targetEntity="MonthlyReport", inversedBy="days")
     * @ORM\JoinColumn(name="month_id", referencedColumnName="id")
     */
    //protected $month;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dayDate
     *
     * @param \DateTime $dayDate
     * @return Day
     */
    public function setDayDate($dayDate)
    {
        $this->dayDate = $dayDate;

        return $this;
    }

    /**
     * Get dayDate
     *
     * @return \DateTime 
     */
    public function getDayDate()
    {
        return $this->dayDate;
    }

    /**
     * Set startHour
     *
     * @param \DateTime $startHour
     * @return Day
     */
    public function setStartHour($startHour)
    {
        $this->startHour = $startHour;

        return $this;
    }

    /**
     * Get startHour
     *
     * @return \DateTime 
     */
    public function getStartHour()
    {
        return $this->startHour;
    }

    /**
     * Set endHour
     *
     * @param \DateTime $endHour
     * @return Day
     */
    public function setEndHour($endHour)
    {
        $this->endHour = $endHour;

        return $this;
    }

    /**
     * Get endHour
     *
     * @return \DateTime 
     */
    public function getEndHour()
    {
        return $this->endHour;
    }

    /**
     * Set hours
     *
     * @param \DateTime $hours
     * @return Day
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return \DateTime 
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Day
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Day
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set month
     *
     * @param integer $month
     * @return Day
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return integer 
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Day
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set user
     *
     * @param \BerG\TimesheetBundle\Entity\User $user
     * @return Day
     */
    public function setUser(\BerG\TimesheetBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BerG\TimesheetBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set week
     *
     * @param \BerG\TimesheetBundle\Entity\Week $week
     * @return Day
     */
    public function setWeek(\BerG\TimesheetBundle\Entity\Week $week = null)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return \BerG\TimesheetBundle\Entity\Week 
     */
    public function getWeek()
    {
        return $this->week;
    }
}
