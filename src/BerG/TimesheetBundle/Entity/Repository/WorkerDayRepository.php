<?php

namespace BerG\TimesheetBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Carbon\Carbon;
use BerG\TimesheetBundle\Entity\WorkerDay;

/**
 * WorkerDayRepository
 */
class WorkerDayRepository extends EntityRepository
{
    public function addWorkingWeek(Carbon $lastLimit, EntityManager $em, User $user)
    {
        //$em = $this->getManager();
        //$user = $this->getUser();
        $weeksheets = [];

        for($i = Carbon::DAYS_PER_WEEK - 1; $i >= 0; --$i)
        {
            $then = $lastLimit->copy()->subDays($i);

            if (!$then->isWeekend() && $then->weekOfYear === $lastLimit->weekOfYear)
            {
                $k = 'wd' . $i;
                ${$k} = new WorkerDay();
                ${$k}->setDayDate($then);
                ${$k}->setWorker($user);
                ${$k}->setWeekOfYear($lastLimit->weekOfYear);
                ${$k}->setMonth($lastLimit->month);
                ${$k}->setYear($lastLimit->year);

                $em->persist(${$k});
                $weeksheets[] = ${$k};
            }
        }

        $em->flush();

        return $weeksheets;
    }

}
