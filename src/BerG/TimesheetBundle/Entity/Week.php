<?php
// src/BerG/TimesheetBundle/Entity/Week.php
namespace BerG\TimesheetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use \BerG\TimesheetBundle\Entity\Day;
use \BerG\TimesheetBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Carbon\Carbon;

/**
 * Week Entity
 *
 * @ORM\Table(name="ts_weeks")
 * @ORM\Entity(repositoryClass="BerG\TimesheetBundle\Entity\Repository\WeekRepository")
 * @ORM\HasLifecycleCallbacks
 *
 */
class Week
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="integer")
     */
    protected $weekOfYear;
    /**
     * @ORM\Column(type="integer")
     */
    protected $year;
    /**
     * @ORM\Column(type="integer")
     */
    //protected $month;
    /**
     * @ORM\OneToMany(targetEntity="Day", mappedBy="week", cascade={"ALL"}, indexBy="dayDate")
     * @Assert\Count(
     *      min = "1",
     *      max = "5",
     *      minMessage = "There should be at least one working day in a submitted week!",
     *      maxMessage = "We don't work more than {{ limit }} days in a week!"
     * )
     */
    protected $days = null;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="weeks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->days = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set weekOfYear
     *
     * @param integer $weekOfYear
     * @return Week
     */
    public function setWeekOfYear($weekOfYear)
    {
        $this->weekOfYear = $weekOfYear;

        return $this;
    }

    /**
     * Get weekOfYear
     *
     * @return integer 
     */
    public function getWeekOfYear()
    {
        return $this->weekOfYear;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Week
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Add days
     *
     * @param \BerG\TimesheetBundle\Entity\Day $days
     * @return Week
     */
    public function addDay(Day $days)
    {
        $this->days[] = $days;

        return $this;
    }

    /**
     * Remove days
     *
     * @param \BerG\TimesheetBundle\Entity\Day $days
     */
    public function removeDay(Day $days)
    {
        $this->days->removeElement($days);
    }

    /**
     * Get days
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Set user
     *
     * @param \BerG\TimesheetBundle\Entity\User $user
     * @return Week
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BerG\TimesheetBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
