<?php

namespace BerG\TimesheetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Exception\ValidatorException;

//use BerG\TimesheetBundle\Validator\Constraints as AcmeAssert;
use BerG\TimesheetBundle\Entity\User;
use Carbon\Carbon;

/**
 * WorkerDay
 *
 * @ORM\Table(name="ts_worker_days")
 * @ORM\Entity(repositoryClass="BerG\TimesheetBundle\Entity\Repository\WorkerDayRepository")
 * @ORM\HasLifecycleCallbacks
 * AcmeAssert\CheckTime
 */
class WorkerDay
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetimetz")
     */
    protected $dayDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="time", nullable=true)
     * @Assert\Type("\Time")
     */
    protected $startHour;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="time", nullable=true)
     * @Assert\Type("\Time")
     */
    protected $endHour;
    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $isFreeDay;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="days")
     * @ORM\JoinColumn(name="worker_id", referencedColumnName="id")
     */
    protected $worker;
    /**
     * @var \Time
     *
     * @ORM\Column(type="time", nullable=true)
     * @Assert\Type("\Time")
     */
    protected $hours;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    /**
     * @ORM\Column(type="integer")
     */
    //protected $month;
    /**
     * @ORM\Column(type="integer")
     */
    protected $year;
    /**
     * @ORM\ManyToOne(targetEntity="Week", inversedBy="days")
     * @ORM\JoinColumn(name="week_id", referencedColumnName="id")
     */
    protected $week;
    /**
     * @ORM\ManyToOne(targetEntity="MonthlyReport", inversedBy="days")
     * @ORM\JoinColumn(name="month_id", referencedColumnName="id")
     */
    //protected $month;

    /**
     *
     * @return boolean
     */
    private function calcHours()
    {
        if (empty($this->hours))
        {
            if (isset($this->startHour) && isset($this->endHour))
            {
                $this->hours = Carbon::instance($this->endHour)->diff(Carbon::instance($this->startHour));
                return true;
            }
        }
        return false;
    }
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('startHour', new Assert\NotBlank(array(
            'message' => 'You must enter start hour'
        )));
        $metadata->addPropertyConstraint('startHour', new Assert\Type(array(
            'type' => '\Time',
            'message' => 'You must enter start hour as time'
        )));
        $metadata->addPropertyConstraint('endHour', new Assert\NotBlank(array(
            'message' => 'You must enter end hour'
        )));
        $metadata->addPropertyConstraint('endHour', new Assert\Type(array(
            'type' => '\Time',
            'message' => 'You must enter end hour as time'
        )));
        $metadata->addPropertyConstraint('hours', new Assert\NotBlank(array(
            'message' => 'Somethign wrong with hours'
        )));
        $metadata->addPropertyConstraint('hours', new Assert\Type(array(
            'type' => '\Time',
            'message' => 'Yet again somethign wrong with hours'
        )));
    }
    /**
     * validation
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function validate()
    {
        if (!($this->startHour instanceof \DateTime)) {
            throw new ValidatorException();
        }
        if (!($this->endHour instanceof \DateTime)) {
            throw new ValidatorException();
        }
        if ($this->endHour < $this->startHour) {
            throw new ValidatorException();
        }
//        dump($this->hours);
//        if (!($this->hours instanceof \DateTime)) {
//            throw new ValidatorException();
//        }
    }
    /**
     * Set created
     * @param \DateTime $created
     * @return WorkerDay
     *
     * @ORM\PrePersist
     */
    public function setCreated()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();

        $this->isFreeDay = false;

        $this->calcHours();

        return $this;
    }
    /**
     * Set updated
     * @param \DateTime $updated
     * @return WorkerDay
     *
     * @ORM\PreUpdate
     */
    public function setUpdated()
    {
        $this->updated = new \DateTime();

        $this->calcHours();

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dayDate
     *
     * @param \DateTime $dayDate
     * @return WorkerDay
     */
    public function setDayDate($dayDate)
    {
        $this->dayDate = $dayDate;

        return $this;
    }

    /**
     * Get dayDate
     *
     * @return \DateTime
     */
    public function getDayDate()
    {
        return $this->dayDate;
    }

    /**
     * Set startHour
     *
     * @param \Time $startHour
     * @return WorkerDay
     */
    public function setStartHour($startHour)
    {
        $this->startHour = $startHour;

        return $this;
    }

    /**
     * Get startHour
     *
     * @return \Time
     */
    public function getStartHour()
    {
        return $this->startHour;
    }

    /**
     * Set endHour
     *
     * @param \Time $endHour
     * @return WorkerDay
     */
    public function setEndHour($endHour)
    {
        $this->endHour = $endHour;

        return $this;
    }

    /**
     * Get endHour
     *
     * @return \Time
     */
    public function getEndHour()
    {
        return $this->endHour;
    }

    /**
     * Set isFreeDay
     *
     * @param boolean $isFreeDay
     * @return WorkerDay
     */
    public function setIsFreeDay($isFreeDay)
    {
        $this->isFreeDay = $isFreeDay;

        return $this;
    }

    /**
     * Get isFreeDay
     *
     * @return boolean
     */
    public function getIsFreeDay()
    {
        return $this->isFreeDay;
    }

    /**
     * Set hours
     *
     * @param \DateTime $hours
     * @return WorkerDay
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return \DateTime
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set month
     *
     * @param integer $month
     * @return WorkerDay
     * ORM\PrePersist
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return integer
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return WorkerDay
     *
     * ORM\PrePersist
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set worker
     *
     * @param \BerG\TimesheetBundle\Entity\User $worker
     * @return WorkerDay
     */
    public function setWorker(\BerG\TimesheetBundle\Entity\User $worker = null)
    {
        $this->worker = $worker;

        return $this;
    }

    /**
     * Get worker
     *
     * @return \BerG\TimesheetBundle\Entity\User
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * Set week
     *
     * @param \BerG\TimesheetBundle\Entity\Week $week
     * @return WorkerDay
     */
    public function setWeek(\BerG\TimesheetBundle\Entity\Week $week = null)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return \BerG\TimesheetBundle\Entity\Week
     */
    public function getWeek()
    {
        return $this->week;
    }
}
