<?php

namespace BerG\TimesheetBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use BerG\TimesheetBundle\Entity\Day;
use Carbon\Carbon;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="BerG\TimesheetBundle\Entity\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * Last submitted day
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    protected $lastSubmittedDay;
    /**
     * @ORM\OneToMany(targetEntity="Day", mappedBy="user", cascade={"ALL"}, indexBy="dayDate")
     */
    protected $days = null;
    /**
     * @ORM\OneToMany(targetEntity="Week", mappedBy="user", cascade={"ALL"}, indexBy="id")
     */
    protected $weeks = null;
    /**
     * @ORM\OneToMany(targetEntity="MonthlyReport", mappedBy="user", cascade={"ALL"}, indexBy="id")
     */
    //protected $months;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    protected $employedFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    protected $employedTo;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $isEmployed;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->days = new ArrayCollection();
        $this->weeks = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employedFrom
     *
     * @param \DateTime $employedFrom
     * @return User
     */
    public function setEmployedFrom($employedFrom)
    {
        $this->employedFrom = $employedFrom;

        return $this;
    }

    /**
     * Get employedFrom
     *
     * @return \DateTime 
     */
    public function getEmployedFrom($m = 0)
    {
        if ($m === 1)
        {
            $fdaw = Carbon::instance($this->employedFrom);
            return $fdaw->isWeekday() ?
                $fdaw : $fdaw->next(Carbon::MONDAY);
        }
        return $this->employedFrom;
    }

    /**
     * Set employedTo
     *
     * @param \DateTime $employedTo
     * @return User
     */
    public function setEmployedTo($employedTo)
    {
        $this->employedTo = $employedTo;

        return $this;
    }

    /**
     * Get employedTo
     *
     * @return \DateTime 
     */
    public function getEmployedTo($m = 0)
    {
        if ($m === 1)
        {
            $ldaw = Carbon::instance($this->employedTo);
            return $ldaw->isWeekday() ?
                $ldaw : $ldaw->previous(Carbon::FRIDAY);
        }
        return $this->employedTo;
    }

    /**
     * Set isEmployed
     *
     * @param boolean $isEmployed
     * @return User
     */
    public function setIsEmployed($isEmployed)
    {
        $this->isEmployed = $isEmployed;

        return $this;
    }

    /**
     * Get isEmployed
     *
     * @return boolean 
     */
    public function getIsEmployed()
    {
        return $this->isEmployed;
    }

    /**
     * Add days
     *
     * @param \BerG\TimesheetBundle\Entity\Day $days
     * @return User
     */
    public function addDay(\BerG\TimesheetBundle\Entity\Day $days)
    {
        $this->days[] = $days;

        return $this;
    }

    /**
     * Remove days
     *
     * @param \BerG\TimesheetBundle\Entity\Day $days
     */
    public function removeDay(\BerG\TimesheetBundle\Entity\Day $days)
    {
        $this->days->removeElement($days);
    }

    /**
     * Get days
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Add weeks
     *
     * @param \BerG\TimesheetBundle\Entity\Week $weeks
     * @return User
     */
    public function addWeek(\BerG\TimesheetBundle\Entity\Week $weeks)
    {
        $this->weeks[] = $weeks;

        return $this;
    }

    /**
     * Remove weeks
     *
     * @param \BerG\TimesheetBundle\Entity\Week $weeks
     */
    public function removeWeek(\BerG\TimesheetBundle\Entity\Week $weeks)
    {
        $this->weeks->removeElement($weeks);
    }

    /**
     * Get weeks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWeeks()
    {
        return $this->weeks;
    }

    /**
     * Set lastSubmittedDay
     *
     * @param \DateTime $lastSubmittedDay
     * @return User
     */
    public function setLastSubmittedDay($lastSubmittedDay)
    {
        $this->lastSubmittedDay = $lastSubmittedDay;

        return $this;
    }

    /**
     * Get lastSubmittedDay
     *
     * @return \DateTime 
     */
    public function getLastSubmittedDay($m = 0)
    {
        if ($m === 1)
        {
            return Carbon::instance($this->lastSubmittedDay);
        }
        return $this->lastSubmittedDay;
    }
    /**
     * Get the first and last days of weeks with open timesheets
     *
     * @return array
     */
    public function getOpenTimesheetsDates()
    {
        $first_day_at_work = $this->getEmployedFrom(1);
        $last_day_at_work = $this->getEmployedTo(1);
        $last_submitted_day = $this->getLastSubmittedDay(1);

        $friday_of_first_unsubmitted_week = $last_submitted_day->copy()
            ->next();
        $friday_of_first_unsubmitted_week = $this->
        getNextFridayIfNotFriday($friday_of_first_unsubmitted_week);

        for (
            $friday_of_unsubmitted_week = $friday_of_first_unsubmitted_week->copy();
            $friday_of_unsubmitted_week->min($last_day_at_work)->endOfDay()->isPast();
            $friday_of_unsubmitted_week->next()
        )
        {
            $monday_of_unsubmitted_week = $friday_of_unsubmitted_week->copy()
                ->previous(Carbon::MONDAY)->max($first_day_at_work);

            $opens[] = [
                'week_id'  => $friday_of_unsubmitted_week->toDateString(),
                'first_day' => $monday_of_unsubmitted_week->toFormattedDateString(),
                'last_day'  => $friday_of_unsubmitted_week->toFormattedDateString(),
            ];

        }

        return $opens;
    }
    public function getNextFridayIfNotFriday($day)
    {
        if ($day->dayOfWeek != Carbon::FRIDAY)
        {
            $day->next(Carbon::FRIDAY);
        }
        return $day;
    }
}
